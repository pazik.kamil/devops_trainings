.. DevOps kurs documentation master file, created by
   sphinx-quickstart on Thu Feb 14 23:25:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DevOps kurs
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   kubernetes/intro
   kubernetes/etcd
   kubernetes/kubectl
   kubernetes/dashboard
   kubernetes/suse_caas
   kubernetes/networking
   docker/advanced



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
