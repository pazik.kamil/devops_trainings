Suse CaaS Platform
-------------------

Komponenty
-----------

* Kubernetes,
* Suse MicroOS,
* Salt, silniki kontenerowe 


Suse MicroOs
-------------

* Tranzakcyjne aktualizacje
* Sprawdzone technologie u podstaw:

  * btrfs,
  * rpm-y
   
Suse **"Transactional Updates"**
_________________________________

* Atomowość - albo cała aktualizacja jest wdrożona, albo nic. Aktualizacje nie powinny popsuć systemu.
* Może być "przywrócony" - ``rolled back``
* Automatyczne sprawdzanie - co 24h lub tj. ustawi użytkownik

