kubectl
========


Informacje 
-----------

.. code:: bash

   kubectl cluster-info

   # wyświetlenie inormacji o "node-ach"

   kubectl get nodes

Deployment
-----------

.. code:: bash

   kubectl run first-deployment --image=python --port=80

   kubectl get pods

   # sprawdzenie portów
   kubectl get svc first-deployment | awk '{print $5}'
