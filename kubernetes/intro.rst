Wstęp kubernetes
=================

* Narzędzie do orchiestracji kontenerów,
* Narzędzie open-source_,
* Napisane w Go,
* Zarządzanie od 2 do 5000 node-ów,
* Self-healing


.. _open-source: https://github.com/kubernetes/kubernetes

Czemu k8s
----------

* Deployment,
* Zarządzanie,
* Skalowanie,
* Zabezpieczenia

.. _pod-reference:

Co to **Pod**
--------------

* Najmniejsza cząstka w k8s którą można opublikować w klastrze - ``deployment``


Master node
------------
* Kontrolują "node-y"   
* Tam się znajduje ``etcd`` oraz ``Api Server`` czy ``Scheduler``

Worker node
------------
* Obrazy są tam ściągane,
* Na tej podstawie tworzą się pody


Namespaces
-----------

* Oddzielają logicznie:

  * Grupy/Użytkowników,
  * Projekty,
  * Zespoły

* Nazwy muszą być unikatowe jedynie w obszarze danej ``namespace``


Service
---------
* Pod musi mieć połączenie z jednego poda do następnego oraz z wewnątrz do nazewnątrz
  
Admin node (Suse)
------------------

Volume
-------
* Storage na np. logi, pliki bazy danych, pliki model danych

Persistent Volume
------------------
* Storage w klastrze - gotowy do użycia, zarządzany przez admina
* Różne implementacje np.

  * NFS,
  * iSCSI,
  * rozwiązania cloudowe


Persistent Volume Claim
------------------------
* "Wniosek" o storage
* Wnioskować można o:

  * Daną przestrzeń,
  * Sposób dostępu

* Sposób w jaki ``Persistent Volume`` jest połączony z aplikacją.


Pet vs Cattle
--------------

* Pody / kontenery to **"bydło"** więc farmerzy(ranchers) nie znają każdej sztuki - oraz nie nazywają ich z imienia


Manifesty
----------

* Dostarczone przez plik,
* Dostarczone przez klienta HTTP,
* Dostarczone przez serwer HTTP

Kubelet
--------
* podstawowy "node agent",

ReplicaSet
----------
* Jego zadanie to utrzymać stabilny stan replik :ref:`podów <pod-reference>`




