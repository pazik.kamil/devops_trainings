ETCD
=====

* Storage typu ``klucz-wartość``,
* Zawiera dane o klastrach,
* Powinno robić się backupy

Odpowiada za
-------------

* Przetrzymywanie konfiguracji,
* Nazwy w sieci

.. hint:: 

   ``ETCDCTL_API=3 ./etcdctl snapshot save snapshot.db --cacert /etc/ssl/etcd/ca.crt --cert /etc/ssl/etcd/client.crt --key /etc/ssl/etcd/client.key``
