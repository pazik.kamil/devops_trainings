Networking
===========

* Flannel

Services
---------

* Pozwalają się łączyć z portami wewnątrz wewnętrznej sieci klastra (miedzy kontenerami).


Usługa może używać

* ClusterIP - ekspozycja dla innych podów w klastrze,
* NodePort - jedno IP zewnętrzne przypisany jeden port,
* LoadBalancer - jedno IP zewnętrzne, które wysyła ruch na odpowiedni port

Kube-DNS
--------

* rozwiązywanie nazw DNS do wewnętrznych i zewnętrznych usług,
* z domysłu w CaaS Platform
